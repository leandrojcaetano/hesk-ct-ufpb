
var jQuery_1_8_1 = $.noConflict(true);

(function($) {
$(document).ready(function() {
        
                function required(obj){
                    $("#" + obj).css("border-color", "#FF0000");
                }
        
                function cleanBorder(obj){
                    $("#" + obj).css("border-color", "#C0C0C0");
                }
        
                this.validaForm = function(){
        
                    if($("#cpf").val() == ""){
                        alert('Por favor, digite o seu CPF!');
                        $("#cpf").focus();
                        required("cpf");
                        return false;
                    }else{
                        cleanBorder("cpf");
                    }
        
                    if($("#name").val() == ""){
                        alert('Por favor, digite o seu Nome!');
                        $("#name").focus();
                        required("name");
                        return false;
                    }else{
                        cleanBorder("name");
                    }
        
                    if($("#email").val() == ""){
                        alert('Sem email cadastrado! Cadastre o email no SIGRH');
                        return false;
                    }else{
                          var regex = /^([a-zA-Z0-9_.+-])+\@(([a-zA-Z0-9-])+\.)+([a-zA-Z0-9]{2,4})+$/;
                          if(! regex.test($("#email").val())){
                              alert('Endere\u00e7o de e-mail inv\u00E1lido! Altere o email no SIGRH');
                              return false;
                          }else{
                                cleanBorder("email");
                          }
                    }
        
                    if($("#setor").val() == ""){
                        alert('Por favor, digite o seu Setor!');
                        $("#setor").focus();
                        required("setor");
                        return false;
                    }else{
                        cleanBorder("setor");
                    }
        
                    if($("#fone").val() == ""){
                        alert('Por favor, digite o seu fone/ramal de contato!');
                        $("#fone").focus();
                        required("fone");
                        return false;
                    }else{
                        cleanBorder("fone");
                    }
        
                    if($("#atendimento").val() == ""){
                        alert('Por favor, digite o local para atendimento!');
                        $("#atendimento").focus();
                        required("atendimento");
                        return false;
                    }else{
                        cleanBorder("atendimento");
                    }
        
                    if($("#solicitacao").val() == ""){
                        alert('Por favor descreva a sua solicitaï¿½ï¿½o');
                        $("#solicitacao").focus();
                        required("solicitacao");
                        return false;
                    }else{
                        cleanBorder("solicitacao");
                    }
        
                    if($("#desc").val() == ""){
                        alert('Por favor, detalhe a sua solicita\u00e7\u00E3o!');
                        $("#desc").focus();
                        required("desc");
                        return false;
                    }else{
                          var frm = document.forms["contact_form"];
        
                        String.prototype.startsWith = function(str){return (this.match("^"+str)==str)};
        
                        if (frm.desc.value.startsWith("Lembramos que atendemos a diversos chamados diariamente, sobre os mais variados assuntos")) {
                            alert('Por favor, detalhe qual a sua solicita\u00e7\u00E3o!');
                            $("#desc").focus();
                            required("desc");
                            return false;
                        }else{
                            cleanBorder("desc");
                        }
                    }
        
                    $("#bt-enviar").hide();
                    $("#container-loading").show();
                    $("#container-loading-texto").show();
        
                    return true;
        
                }
        
            $("#cpf").keypress(digitos);
            $("#cpf").blur(function()
            {
                if($("#cpf").val()){
        
                    $.ajaxSetup({ cache: false });
        
                                var cpf=$("#cpf").attr("value");
        
                    $.ajax({
                        async: "false",
                        dataType: "json",
                        url: "https://sigrh.ufpb.br/sigrh/rest/dadosjson/" + cpf,
        
                        success: function(dados) {
                                                if ( dados.nome === "" ) {
                                alert("Servidor não encontrado ou inexistente! Por favor, continue o preenchimento dos dados manualmente.");
                                //location.reload(true);
                                clearForm();
        
                            } else {
                                $('#cpf').css("border", "#CCC solid 1px");
                                $('#name').css("background-color", "");
                                $('#nome').attr('readonly');
                                $('#setor').css("background-color", "#DCDCDC");
                                $('#setor').attr('readonly');
                                $('#fone').removeAttr('readonly');
                                $('#fone').css("background-color", "#ffffff");
                                $('#solicitacao').removeAttr('readonly');
                                $('#solicitacao').css("background-color", "#ffffff");
                                $('#atendimento').removeAttr('readonly');
                                $('#atendimento').css("background-color", "#ffffff");
                                $('#desc').removeAttr('readonly');
                                $('#desc').css("background-color", "#ffffff");
                                $('#desc').css("color", "#000000");
                                if (dados.setor === "")	{
                                    $('#setor').val('SETOR NAO INFORMADO');
                                } else {
                                    $("#setor").val(dados.setor);
                                }
                                $("#name").val(dados.nome);
                                $("#email").val(dados.email);
                                $("#fone").val(dados.telefone);
                                $("#login").val(dados.login);
                            }
                        },
                        error: function (xhr, ajaxOptions, thrownError){
                                            if ( $('#cpf').val() === ""){
                                alert("Por favor, digite o nï¿½mero do cpf! ");
                                $('#name').location.reload(true);
        
                            } else {
                                $('#name').css("background-color", "#ffffff");
                                $('#name').removeAttr('readonly');
                                $('#setor').css("background-color", "#ffffff");
                                $('#setor').removeAttr('readonly');
                                $('#fone').css("background-color", "#ffffff");
                                $('#fone').removeAttr('readonly');
                                $('#solicitacao').css("background-color", "#ffffff");
                                $('#solicitacao').removeAttr('readonly');
                                $('#atendimento').css("background-color", "#ffffff");
                                $('#atendimento').removeAttr('readonly');
                                $('#desc').css("background-color", "#ffffff");
                                $('#desc').removeAttr('readonly');
                            }
                        },
                        timeout: 10000 			});
        
                }else{
                    $('#cpf').css("border", "#FF0000 solid 1px");
                    clearForm();
                }
        
            });
        
            clearForm = function(){
                $("#cpf").val('');
                $("#name").val('');
                $("#login").val('');
                $("#email").val('');
                $('#email').css("background-color", "");
                $("#setor").val('');
                $('#setor').css("background-color", "#DCDCDC");
                $('#setor').attr('readonly', 'readonly');
                $("#atendimento").val('');
                $('#atendimento').css("background-color", "#DCDCDC");
                $('#atendimento').attr('readonly', 'readonly');
                $("#fone").val('');
                $('#fone').css("background-color", "#DCDCDC");
                $('#fone').attr('readonly', 'readonly');
                $("#solicitacao").val('');
                $('#solicitacao').css("background-color", "#DCDCDC");
                $('#solicitacao').attr('readonly', 'readonly');
                $("#desc").val('');
                $('#desc').css("background-color", "#DCDCDC");
                $('#desc').attr('readonly', 'readonly');
                $("#fone").val('');
                $("#solicitacao").val('');
                $("#atendimento").val('');
                $("#desc").val('');
            };
        });


        function somenteNumero(e){
            var tecla=(window.event)?event.keyCode:e.which;
            if((tecla>47 && tecla<58)) return true;
            else{
                if (tecla==8 || tecla==0) return true;
            else  return false;
            }
        }
        
        function adiciona(){
            document.contact_form.atendimento.value = document.contact_form.nomeLocal.value.replace(/[\*\.]/g , " ");
            document.contact_form.setorCorreto.value = "verdadeiro";
            document.contact_form.nomeLocal.remove();
        }
        
        function formatar(mascara, documento){
          var i = documento.value.length;
          var saida = mascara.substring(0,1);
          var texto = mascara.substring(i);
        
          if (texto.substring(0,1) != saida){
                    documento.value += texto.substring(0,1);
          }
        
        }
        
        function adicionaAnexo(el) {
            var display2 = document.getElementById(el).style.display;
            var display3 = document.getElementById(el).style.display;
            document.getElementById(el).style.display = 'block';
            if(display2 == "block"){
                document.getElementById("arquivo3").style.display = 'block';
            }
        }
        
        function removeAnexo(el) {
            var display2 = document.getElementById(el).style.display;
            var display3 = document.getElementById(el).style.display;
            document.getElementById(el).style.display = 'none';
            document.getElementById(el).value = "";
            if(display3 == "none"){
                document.getElementById("arquivo2").style.display = 'none';
                document.getElementById("arquivo2").value = "";
            }
        }
        
        //--->Funï¿½ï¿½o para verificar se o valor digitado nï¿½mero...<---
        function digitos(event){
                if (window.event) {
                        // IE
                        key = event.keyCode;
                } else if ( event.which ) {
                        // netscape
                        key = event.which;
                }
                if ( key != 8 || key != 13 || key < 48 || key > 57 )
                        return ( ( ( key > 47 ) && ( key < 58 ) ) || ( key == 8 ) || ( key == 13 ) );
                return true;
        }
	
})(jQuery_1_8_1); 
</script>

<script type="text/javascript">
    $(document).ready(function(){
        //  Focus auto-focus fields
        $('.auto-focus:first').focus();

        //  Initialize auto-hint fields
        $('INPUT.auto-hint, TEXTAREA.auto-hint').focus(function(){
            if($(this).val() == $(this).attr('title')){
                $(this).val('');
                $(this).removeClass('auto-hint');
            }
        });

        $('INPUT.auto-hint, TEXTAREA.auto-hint').blur(function(){
            if($(this).val() == '' && $(this).attr('title') != ''){
                $(this).val($(this).attr('title'));
                $(this).addClass('auto-hint');
            }
        });

        $('INPUT.auto-hint, TEXTAREA.auto-hint').each(function(){
            if($(this).attr('title') == ''){ return; }
            if($(this).val() == ''){ $(this).val($(this).attr('title')); }
            else { $(this).removeClass('auto-hint'); }
        });

        $("#abrir-chamado").click(function(){
			$(".abertura").show("slow");
        });
    });


    function submitform()
    {
      document.contact_form.submit();
    }