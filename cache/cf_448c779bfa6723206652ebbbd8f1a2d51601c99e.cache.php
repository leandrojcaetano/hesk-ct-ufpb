<?php if (!defined('IN_SCRIPT')) {die();} $hesk_settings['custom_fields']=array (
  'custom15' => 
  array (
    'use' => '1',
    'place' => '0',
    'type' => 'text',
    'req' => '0',
    'category' => 
    array (
    ),
    'name' => 'Contato Alternativo [Telefone/Whatsapp ou E-mail]',
    'value' => 
    array (
      'max_length' => 255,
      'default_value' => '',
    ),
    'order' => '10',
    'title' => 'Contato Alternativo [Telefone/...',
    'name:' => 'Contato Alternativo [Telefone/Whatsapp ou E-mail]:',
  ),
  'custom10' => 
  array (
    'use' => '1',
    'place' => '0',
    'type' => 'radio',
    'req' => '2',
    'category' => 
    array (
      0 => '4',
      1 => '46',
      2 => '39',
      3 => '2',
      4 => '20',
      5 => '21',
      6 => '22',
      7 => '23',
      8 => '24',
      9 => '25',
      10 => '26',
      11 => '27',
      12 => '28',
      13 => '29',
      14 => '53',
      15 => '41',
      16 => '54',
      17 => '58',
      18 => '13',
      19 => '14',
      20 => '15',
      21 => '16',
      22 => '17',
      23 => '18',
      24 => '19',
      25 => '52',
      26 => '47',
      27 => '48',
      28 => '1',
      29 => '9',
      30 => '49',
      31 => '43',
      32 => '40',
      33 => '44',
      34 => '30',
      35 => '31',
      36 => '32',
      37 => '33',
      38 => '34',
      39 => '35',
      40 => '36',
      41 => '37',
      42 => '5',
      43 => '45',
      44 => '56',
      45 => '59',
    ),
    'name' => 'Perfil do Usuário',
    'value' => 
    array (
      'radio_options' => 
      array (
        0 => 'Discente',
        1 => 'Docente',
        2 => 'Técnico-Administrativo',
        3 => 'Visitante',
      ),
      'no_default' => 1,
    ),
    'order' => '20',
    'title' => 'Perfil do Usuário',
    'name:' => 'Perfil do Usuário:',
  ),
  'custom5' => 
  array (
    'use' => '1',
    'place' => '0',
    'type' => 'text',
    'req' => '0',
    'category' => 
    array (
      0 => '1',
    ),
    'name' => 'Patrimônio',
    'value' => 
    array (
      'max_length' => 255,
      'default_value' => '',
    ),
    'order' => '30',
    'title' => 'Patrimônio',
    'name:' => 'Patrimônio:',
  ),
  'custom2' => 
  array (
    'use' => '2',
    'place' => '0',
    'type' => 'radio',
    'req' => '2',
    'category' => 
    array (
      0 => '1',
    ),
    'name' => 'Tipo do Equipamento',
    'value' => 
    array (
      'radio_options' => 
      array (
        0 => 'Computador',
        1 => 'Datashow',
        2 => 'Estabilizador',
        3 => 'Impressora',
        4 => 'Monitor',
        5 => 'Notebook',
        6 => 'Nobreak',
        7 => 'Roteador',
        8 => '- Outros -',
      ),
      'no_default' => 0,
    ),
    'order' => '40',
    'title' => 'Tipo do Equipamento',
    'name:' => 'Tipo do Equipamento:',
  ),
  'custom4' => 
  array (
    'use' => '2',
    'place' => '0',
    'type' => 'radio',
    'req' => '0',
    'category' => 
    array (
      0 => '3',
    ),
    'name' => 'Servidor',
    'value' => 
    array (
      'radio_options' => 
      array (
        0 => 'Insalubridade',
        1 => 'Periculosidade',
        2 => 'Férias',
        3 => 'Décimo Terceiro',
      ),
      'no_default' => 0,
    ),
    'order' => '50',
    'title' => 'Servidor',
    'name:' => 'Servidor:',
  ),
  'custom1' => 
  array (
    'use' => '1',
    'place' => '0',
    'type' => 'radio',
    'req' => '2',
    'category' => 
    array (
      0 => '1',
    ),
    'name' => 'Tipo de Atendimento (Informática)',
    'value' => 
    array (
      'radio_options' => 
      array (
        0 => 'Manutenção / Suporte',
        1 => 'Páginas / Websites',
        2 => 'Redes / Internet',
        3 => 'Webmail',
        4 => '-- Outros --',
      ),
      'no_default' => 1,
    ),
    'order' => '60',
    'title' => 'Tipo de Atendimento (Informáti...',
    'name:' => 'Tipo de Atendimento (Informática):',
  ),
  'custom3' => 
  array (
    'use' => '1',
    'place' => '0',
    'type' => 'select',
    'req' => '2',
    'category' => 
    array (
      0 => '4',
      1 => '46',
      2 => '39',
      3 => '2',
      4 => '20',
      5 => '21',
      6 => '22',
      7 => '23',
      8 => '24',
      9 => '25',
      10 => '26',
      11 => '27',
      12 => '28',
      13 => '29',
      14 => '53',
      15 => '41',
      16 => '54',
      17 => '58',
      18 => '13',
      19 => '14',
      20 => '15',
      21 => '16',
      22 => '17',
      23 => '18',
      24 => '19',
      25 => '52',
      26 => '47',
      27 => '48',
      28 => '1',
      29 => '9',
      30 => '49',
      31 => '43',
      32 => '40',
      33 => '44',
      34 => '30',
      35 => '31',
      36 => '32',
      37 => '33',
      38 => '34',
      39 => '35',
      40 => '36',
      41 => '37',
      42 => '5',
      43 => '45',
      44 => '59',
    ),
    'name' => 'Setor de Origem',
    'value' => 
    array (
      'show_select' => 1,
      'select_options' => 
      array (
        0 => '[Externo]',
        1 => 'Administrativo do CT',
        2 => 'Almoxarifado e Patrimônio',
        3 => 'Aquisições e Contratos',
        4 => 'Assessoria Administrativa',
        5 => 'Assessoria de Extensão',
        6 => 'Assessoria de Graduação',
        7 => 'Assessoria de Infraestrutura',
        8 => 'Assessoria de Pós-Graduação e Pesquisa',
        9 => 'Assessoria de Relações Institucionais',
        10 => 'Assessorias Acadêmicas',
        11 => 'Comunicação Institucional do CT',
        12 => 'Coordenação do curso de Arquitetura e Urbanismo - CCAU',
        13 => 'Coordenação do curso de Engenharia Ambiental - CCEAM',
        14 => 'Coordenação do curso de Engenharia Civil - CCGEC',
        15 => 'Coordenação do curso de Engenharia de Alimentos - CGCEA',
        16 => 'Coordenação do curso de Engenharia de Materiais - CGEMAT',
        17 => 'Coordenação do curso de Engenharia de Produção - CGEP',
        18 => 'Coordenação do curso de Engenharia de Produção Mecânica - CGEPM',
        19 => 'Coordenação do curso de Engenharia Mecânica - CCGEM',
        20 => 'Coordenação do curso de Engenharia Química - CGCEQ',
        21 => 'Coordenação do curso de Química Industrial - CGCQI',
        22 => 'Coordenações de Curso de Graduação',
        23 => 'Coordenações de Programas de Pós-',
        24 => 'Departamento de Arquitetura e Urbanismo - DAU',
        25 => 'Departamento de Engenharia Civil e Ambiental - DECA',
        26 => 'Departamento de Engenharia de Alimentos - DEA',
        27 => 'Departamento de Engenharia de Materiais - DEMAT',
        28 => 'Departamento de Engenharia de Produção - DEP',
        29 => 'Departamento de Engenharia Mecânica - DEM',
        30 => 'Departamento de Engenharia Química - DEQ',
        31 => 'Direção do CT',
        32 => 'Disponibilidade de Meios',
        33 => 'Documentação e Arquivo',
        34 => 'Eventos',
        35 => 'Gestão Acadêmica',
        36 => 'Gestão Administrativa',
        37 => 'Gestão da Infraestrutura',
        38 => 'Gestão de Pessoas',
        39 => 'Informática do CT',
        40 => 'Laboratório de Ensaios Mecânicos - LEM',
        41 => 'Laboratório de Tratamento Térmico - LTT',
        42 => 'Laboratório de Topografia - LABTOP',
        43 => 'Logística',
        44 => 'Manutenção predial e de equipamentos',
        45 => 'Meio Ambiente',
        46 => 'Programa de Pós-Graduação em Arquitetura e Urbanismo - PPGAU',
        47 => 'Programa de Pós-Graduação em Ciência e Engenharia de Materiais - PPCEM',
        48 => 'Programa de Pós-Graduação em Ciência e Tecnologia de Alimentos - PPGCTA',
        49 => 'Programa de Pós-Graduação em Engenharia Civil e Ambiental - PPGECAM',
        50 => 'Programa de Pós-Graduação em Engenharia de Produção - PPGEP',
        51 => 'Programa de Pós-Graduação em Engenharia de Produção e Sistemas - PPGEPS',
        52 => 'Programa de Pós-Graduação em Engenharia Mecânica - PPGEM',
        53 => 'Programa de Pós-Graduação em Engenharia Química - PPGEQ',
        54 => 'Projetos Arquitetônicos e de Engenharia',
        55 => 'Secretaria do CT',
        56 => 'Segurança',
        57 => 'Serviços Gerais',
      ),
    ),
    'order' => '70',
    'title' => 'Setor de Origem',
    'name:' => 'Setor de Origem:',
  ),
  'custom6' => 
  array (
    'use' => '1',
    'place' => '0',
    'type' => 'radio',
    'req' => '2',
    'category' => 
    array (
      0 => '5',
    ),
    'name' => 'Tipo de Atendimento (Secretaria de Centro)',
    'value' => 
    array (
      'radio_options' => 
      array (
        0 => 'Direção',
        1 => 'Secretaria',
        2 => 'Eventos',
        3 => '-- Outros --',
      ),
      'no_default' => 1,
    ),
    'order' => '80',
    'title' => 'Tipo de Atendimento (Secretari...',
    'name:' => 'Tipo de Atendimento (Secretaria de Centro):',
  ),
  'custom7' => 
  array (
    'use' => '1',
    'place' => '0',
    'type' => 'radio',
    'req' => '2',
    'category' => 
    array (
      0 => '9',
    ),
    'name' => 'Tipo de Atendimento (Infraestrutura)',
    'value' => 
    array (
      'radio_options' => 
      array (
        0 => 'Manutenção',
        1 => 'Serviços Gerais',
        2 => 'Patrimônio',
        3 => '-- Outros --',
      ),
      'no_default' => 1,
    ),
    'order' => '90',
    'title' => 'Tipo de Atendimento (Infraestr...',
    'name:' => 'Tipo de Atendimento (Infraestrutura):',
  ),
  'custom11' => 
  array (
    'use' => '1',
    'place' => '0',
    'type' => 'radio',
    'req' => '2',
    'category' => 
    array (
      0 => '38',
    ),
    'name' => 'Tipo de Atendimento (Setor Administrativo)',
    'value' => 
    array (
      'radio_options' => 
      array (
        0 => 'Aquisição e Contratos',
        1 => 'Documentação e Arquivo',
        2 => 'Gestão de Pessoas',
        3 => 'Logística',
        4 => '-- Outros --',
      ),
      'no_default' => 1,
    ),
    'order' => '100',
    'title' => 'Tipo de Atendimento (Setor Adm...',
    'name:' => 'Tipo de Atendimento (Setor Administrativo):',
  ),
  'custom12' => 
  array (
    'use' => '1',
    'place' => '0',
    'type' => 'radio',
    'req' => '2',
    'category' => 
    array (
      0 => '39',
    ),
    'name' => 'Tipo de Atendimento (Assessorias)',
    'value' => 
    array (
      'radio_options' => 
      array (
        0 => 'Assessoria de Graduação',
        1 => 'Assessoria de Pós-graduação e pesquisa',
        2 => 'Assessoria de Extensão',
        3 => 'Assessoria de Integração Institucional',
        4 => 'Assessoria para assuntos administrativos',
        5 => 'Assessoria de Infraestrutura',
        6 => '-- Outros --',
      ),
      'no_default' => 1,
    ),
    'order' => '110',
    'title' => 'Tipo de Atendimento (Assessori...',
    'name:' => 'Tipo de Atendimento (Assessorias):',
  ),
  'custom16' => 
  array (
    'use' => '1',
    'place' => '0',
    'type' => 'select',
    'req' => '0',
    'category' => 
    array (
      0 => '56',
    ),
    'name' => 'Local do Sanitário',
    'value' => 
    array (
      'show_select' => 0,
      'select_options' => 
      array (
        0 => 'Administrativo',
        1 => 'Ambiente dos professores',
        2 => 'Biblioteca Setorial',
        3 => 'Bloco Multimídias',
        4 => 'Centro Acadêmico de Engenharia Civil',
        5 => 'Centro Acadêmico',
        6 => 'CTA',
        7 => 'CTD',
        8 => 'CTDEM LABES',
        9 => 'CTE',
        10 => 'CTF',
        11 => 'CTG',
        12 => 'CTH',
        13 => 'CTJ',
        14 => 'CT-KLM',
        15 => 'CTN',
        16 => 'Engenharia de Alimentos',
        17 => 'Engenharia Elétrica',
        18 => 'LABEME',
        19 => 'LENHS',
        20 => 'NEPEM',
        21 => 'Oficina Mecânica',
      ),
    ),
    'order' => '120',
    'title' => 'Local do Sanitário',
    'name:' => 'Local do Sanitário:',
  ),
  'custom9' => 
  array (
    'use' => '1',
    'place' => '0',
    'type' => 'radio',
    'req' => '0',
    'category' => 
    array (
      0 => '56',
    ),
    'name' => 'Pavimento',
    'value' => 
    array (
      'radio_options' => 
      array (
        0 => 'Térreo',
        1 => '1º Andar',
        2 => '2º Andar',
      ),
      'no_default' => 0,
    ),
    'order' => '130',
    'title' => 'Pavimento',
    'name:' => 'Pavimento:',
  ),
  'custom8' => 
  array (
    'use' => '1',
    'place' => '0',
    'type' => 'radio',
    'req' => '0',
    'category' => 
    array (
      0 => '56',
    ),
    'name' => 'Gênero',
    'value' => 
    array (
      'radio_options' => 
      array (
        0 => 'Masculino',
        1 => 'Feminino',
        2 => 'Uso Geral',
      ),
      'no_default' => 1,
    ),
    'order' => '140',
    'title' => 'Gênero',
    'name:' => 'Gênero:',
  ),
  'custom14' => 
  array (
    'use' => '1',
    'place' => '0',
    'type' => 'hidden',
    'req' => '0',
    'category' => 
    array (
      0 => '56',
    ),
    'name' => 'Código Interno',
    'value' => 
    array (
      'max_length' => 10,
      'default_value' => 'web',
    ),
    'order' => '150',
    'title' => 'Código Interno',
    'name:' => 'Código Interno:',
  ),
  'custom13' => 
  array (
    'use' => '1',
    'place' => '0',
    'type' => 'checkbox',
    'req' => '2',
    'category' => 
    array (
      0 => '56',
    ),
    'name' => 'Problemas Principais',
    'value' => 
    array (
      'checkbox_options' => 
      array (
        0 => 'Limpeza do Ambiente',
        1 => 'Falta de Água',
        2 => 'Reposição Papel Higiênico',
        3 => 'Reposição Papel Toalha',
        4 => 'Reposição Sabonete',
        5 => 'Esgotamento Inadequado/Entupimento',
        6 => 'Torneiras Precisando de Troca',
        7 => 'Vasos Sanitários precisando de troca',
        8 => 'Outros',
      ),
    ),
    'order' => '160',
    'title' => 'Problemas Principais',
    'name:' => 'Problemas Principais:',
  ),
  'custom17' => 
  array (
    'use' => '2',
    'place' => '0',
    'type' => 'text',
    'req' => '0',
    'category' => 
    array (
      0 => '1',
    ),
    'name' => 'Prezado usuário, cada solicitação de atendimento deverá ser feita para um problema específico. Sendo necessário, solicite mais de um atendimento.',
    'value' => 
    array (
      'max_length' => 255,
      'default_value' => '',
    ),
    'order' => '170',
    'title' => 'Prezado usuário, cada solicita...',
    'name:' => 'Prezado usuário, cada solicitação de atendimento deverá ser feita para um problema específico. Sendo necessário, solicite mais de um atendimento.',
  ),
);