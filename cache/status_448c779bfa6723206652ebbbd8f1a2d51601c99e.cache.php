<?php if (!defined('IN_SCRIPT')) {die();} $hesk_settings['statuses']=array (
  0 => 
  array (
    'name' => 'Novo',
    'class' => 'open',
  ),
  1 => 
  array (
    'name' => 'Cliente Respondeu',
    'class' => 'waitingreply',
  ),
  2 => 
  array (
    'name' => 'Respondido',
    'class' => 'replied',
  ),
  3 => 
  array (
    'name' => 'Resolvido',
    'class' => 'resolved',
  ),
  4 => 
  array (
    'name' => 'Em progresso',
    'class' => 'inprogress',
  ),
  5 => 
  array (
    'name' => 'Em espera',
    'class' => 'onhold',
  ),
  6 => 
  array (
    'name' => 'Pendência externa',
    'color' => '#ff2711',
    'can_customers_change' => '0',
  ),
  7 => 
  array (
    'name' => 'Aguardando Retirada',
    'color' => '#008000',
    'can_customers_change' => '0',
  ),
  8 => 
  array (
    'name' => 'Aguardando Entrada',
    'color' => '#adadad',
    'can_customers_change' => '0',
  ),
  9 => 
  array (
    'name' => 'Equipamento recebido',
    'color' => '#000000',
    'can_customers_change' => '0',
  ),
);