<p>Ol&aacute;,</p>
<p>Um novo atendimento foi enviado.&nbsp;Detalhes do atendimento:</p>
<p><strong>C&oacute;digo de identifica&ccedil;&atilde;o:</strong> %%TRACK_ID%%</p>
<p><strong>Solicitante</strong>: %%NAME%%<br /> <strong>Assunto do atendimento:</strong> %%SUBJECT%%<br /> <strong>Mensagem:</strong> %%MESSAGE%%</p>
<p>Voc&ecirc; pode gerenciar este atendimento aqui:<br /> %%TRACK_URL%%</p>
<p><img src="https://qrcode.ct.ufpb.br/generate.php?code=%%TRACK_URL%%" width="200" height="200" /></p>
<p>Atenciosamente,</p>
<p>%%SITE_TITLE%%<br /> %%SITE_URL%%</p>