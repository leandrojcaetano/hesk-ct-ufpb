<p>Ol&aacute;,</p>

<p>Um novo atendimento foi movido para sua categoria. Detalhes do atendimento:</p>

<p>Assunto do atendimento: %%SUBJECT%%<br>
C&oacute;digo de identifica&ccedil;&atilde;o: %%TRACK_ID%%</p>

<p>Voc&ecirc; pode gerenciar este atendimento aqui:<br>
%%TRACK_URL%%</p>

<p>
Atenciosamente,</p>

<p>%%SITE_TITLE%%<br>
%%SITE_URL%%</p>